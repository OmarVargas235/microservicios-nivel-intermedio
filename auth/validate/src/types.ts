type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export namespace Create {

    export interface Request {
        fullName: string;
        phone: string;
        image: string;
        userName?: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Delete {

    export interface Request {
        userName: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request
        message?: string;
    }
}

export namespace Update {

    export interface Request {
        fullName?: string;
        phone?: string;
        image?: string;
        userName: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request
        message?: string;
    }
}

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;
        state?: boolean;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace FindOne {

    export interface Request {
        userName: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request
        message?: string;
    }
}