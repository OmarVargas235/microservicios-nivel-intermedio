import * as T from './types';
import Joi from 'joi';
// import * as Models from '../models';
// import { InternalError } from '../settings';

export async function create(params: T.Create.Request): Promise<T.Create.Response> {

    try {

        const schema = Joi.object({
            userName: Joi.string().required(),
            fullName: Joi.string().required(),
            phone: Joi.number().required(),
            image: Joi.string().uri().required(),
        });

        const result = await schema.validateAsync(params);

        if (params.userName === 'admin' || 'root' || 'su') {

            throw { statusCode: 'error', message: "Nombre reservado para uso interno" }
        }

        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {

    try {

        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}

export async function update(params: T.Update.Request): Promise<T.Update.Response> {

    try {

        const schema = Joi.object({
            userName: Joi.string().required(),
            fullName: Joi.string(),
            phone: Joi.number(),
            image: Joi.string().uri(),
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}

export async function view(params: T.View.Request): Promise<T.View.Response> {

    try {

        const schema = Joi.object({
            offset: Joi.number(),
            limit: Joi.number(),
            state: Joi.boolean(),
        });

        const result = await schema.validateAsync(params);
        
        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}

export async function findOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {

    try {

        const schema = Joi.object({
            userName: Joi.number().required(),
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}