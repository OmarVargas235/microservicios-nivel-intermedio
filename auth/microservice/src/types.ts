import * as S from 'sequelize';

type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export interface IProps {
    fullName: string;
    phone: string;
    image: string;
}

export interface IUser extends IProps {
    id: number;
}

export namespace Models {

    export interface ModelAttributes {
        id?: number;
        fullName?: string,
        image?: string,
        phone?: string,
        createdAt?: string;
        uodatedAt?: string;
        userName?: string;
        password?: string;
        state?: boolean;
    }

    export const attributes = ['id', 'fullName', 'image', 'phone', 'createdAt', 'uodatedAt', 'userName', 'password', 'state'] as const;

    export type Attributes = typeof attributes[number];
    export type Where = S.WhereOptions<ModelAttributes>;
    
    export interface Model extends S.Model<ModelAttributes> {
        // country: string;
    }

    export interface IPaginate {
        data: ModelAttributes[];
        itemCount: number;
        pageCount: number;
    }

    export namespace SyncDB {

        export interface Request extends S.SyncOptions {

        }

        export interface Response {
            statusCode: StatusCode;
            data?: Model;
            message?: string;
        }
    }

    export namespace Count {

        export interface Request extends Omit<S.CountOptions<ModelAttributes>, "group"> {
        }

        export interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }

    export namespace Create {

        export interface Request extends ModelAttributes {
        }

        export interface Opts extends S.CreateOptions<ModelAttributes> {
        }

        export interface Response {
            statusCode: StatusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }

    export namespace Delete {

        export interface Opts extends S.DestroyOptions<ModelAttributes> {
        }

        export interface Response {
            statusCode: StatusCode;
            data?: number;
            message?: string;
        }
    }

    export namespace FindAndCountAll {

        export interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group"> {
        }

        export interface Response {
            statusCode: StatusCode;
            data?: IPaginate;
            message?: string;
        }
    }

    export namespace FindOne {

        export interface Opts extends S.FindOptions<ModelAttributes> {
        }

        export interface Response {
            statusCode: StatusCode;
            data?: ModelAttributes;
            message?: string;
        }
    }

    export namespace Update {

        export interface Request extends ModelAttributes {
        }

        export interface Opts extends S.UpdateOptions<ModelAttributes> {
        }

        export interface Response {
            statusCode: StatusCode;
            data?: [number, ModelAttributes[]];
            message?: string;
        }
    }
}

export namespace Controllers {

    export namespace Publish {

        export interface Request {
            channel: string;
            instance: string;
        }
    
        export interface Response {
            statusCode: StatusCode;
            data?: Request;
            message?: string;
        }
    }
}

export namespace Services {

    export namespace Create {

        export interface Request {
            fullName: string;
            phone: string;
            image: string;
            userName?: string;
        }
    
        export interface Response {
            statusCode: StatusCode;
            data?: Models.ModelAttributes | string;
            message?: string;
        }
    }

    export namespace Delete {

        export interface Request {
            userName: string;
        }
    
        export interface Response {
            statusCode: StatusCode;
            data?: string | Models.ModelAttributes;
            message?: string;
        }
    }

    export namespace Update {

        export interface Request {
            fullName?: string;
            phone?: string;
            image?: string;
            userName: string;
        }
    
        export interface Response {
            statusCode: StatusCode;
            data?: string | Models.ModelAttributes;
            message?: string;
        }
    }

    export namespace View {

        export interface Request {
            offset?: number;
            limit?: number;
            state?: boolean;
        }
    
        export interface Response {
            statusCode: StatusCode;
            data?: Models.IPaginate;
            message?: string;
        }
    }
    
    export namespace FindOne {

        export interface Request {
            userName: string;
        }
    
        export interface Response {
            statusCode: StatusCode;
            data?: string | Models.ModelAttributes;
            message?: string;
        }
    }
}

export namespace Adapters {

    export const endpoint = [ 'create', 'update', 'delete', 'findOne', 'view' ] as const;

    export type Endpoint = typeof endpoint[number];

    export namespace BullConn {

        export interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }
}

export namespace Settings {

    export const attributes = [ 'create', 'update', 'delete', 'findOne', 'view' ] as const;

    export interface REDIS {
        host: string,
        port: number,
        password: string,
    }
}