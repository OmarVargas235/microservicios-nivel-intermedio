import { InternalError, redisClient } from '../settings';
import { Controllers as T } from '../types';

export const Publish = async (props: T.Publish.Request) : Promise<T.Publish.Response> => {
    
    try {

        if (!redisClient.isOpen) await redisClient.connect();

        await redisClient.publish(props.channel, props.instance);

        return { statusCode: 'success', data: props };
    
    } catch(err) {

        console.log({ step: 'controller Publish', err: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}