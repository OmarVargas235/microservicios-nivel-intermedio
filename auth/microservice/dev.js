const { SyncDB, run } = require('./dist');

(async () => {
    
    try {

        const result = await SyncDB({ force: false, logging: true });

        if (result.statusCode !== 'success') throw result.message;

        await run();

    } catch(err) {

        console.log(err);
    }

})();