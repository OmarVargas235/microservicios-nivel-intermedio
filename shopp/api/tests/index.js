const api = require('../dist');

const redis = {
    local: {
        host: '192.168.100.213',
        port: 6379,
        // password: undefined,
    },
    remote: {
        host: 'sandbox.jwit.ar',
        port: 6379,
        password: 'iaby7oadkiyjdumarjql6'
    }
}

const environmet = redis.local;

const users = [
    {
        fullName: 'Omar',
        image: 'imagen',
        phone: '261231231231',
        userName: 'OmarVargas235'
    },
    {
        fullName: 'Omar',
        image: 'imagen',
        phone: '261231231231',
        userName: 'OmarVargas235'
    },
];

async function create(user) {

    try {

        const result = await api.Create(user, environmet);

        console.log(result);

    } catch(err) {

        console.error(err);
    }
}

async function del(userName) {
    
    try {

        const result = await api.Delete({ userName }, environmet);

        console.log(result);

    } catch(err) {

        console.error(err);
    }
}

async function update(params) {
    
    try {

        const result = await api.Update(params, environmet);

        console.log(result);

    } catch(err) {

        console.error(err);
    }
}

async function findOne(params) {
    
    try {

        const result = await api.FindOne(params, environmet);

        console.log(result);

    } catch(err) {

        console.error(err);
    }
}

async function view(params) {
    
    try {

        // del(users[0].userName);
        const result = await api.View(params, environmet);

        console.log(result);

    } catch(err) {

        console.error(err);
    }
}

const main = async () => {

    try {

        await view();

    } catch(err) {

        console.log(err);
    }
}

main();