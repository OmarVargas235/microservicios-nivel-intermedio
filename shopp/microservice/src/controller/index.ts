import { InternalError, redisClient, RedisOptsQueue as Opts } from '../settings';
import { Controllers as T } from '../types';

import * as apiUser from 'api-users';
import * as apiProduct from 'api-product';

export const Publish = async (props: T.Publish.Request) : Promise<T.Publish.Response> => {
    
    try {

        if (!redisClient.isOpen) await redisClient.connect();

        await redisClient.publish(props.channel, props.instance);

        return { statusCode: 'success', data: props };
    
    } catch(err) {

        console.log({ step: 'controller Publish', err: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export const ValidateUser = async (props: T.ValidateUser.Request) : Promise<T.ValidateUser.Response> => {
    
    try {

        const { statusCode, data, message } = await apiUser.FindOne({ id: props.user }, Opts.redis);

        if (statusCode !== 'success') {

            switch(statusCode) {

                case 'notFound': throw { statusCode: 'success', message: "No existe el usuario que intentamos validar" };

                default: throw { statusCode, message };
            }
        }

        return { statusCode: 'success', data };
    
    } catch(err) {

        console.log({ step: 'controller ValidateUser', err: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export const ValidateProduct = async (props: T.ValidateProduct.Request) : Promise<T.ValidateProduct.Response> => {
    
    try {

        if (!redisClient.isOpen) await redisClient.connect();
        
        const { statusCode, data, message } = await apiProduct.FindOne({ id: props.product }, Opts.redis);

        if (statusCode !== 'success') {

            switch(statusCode) {

                case 'notFound': throw { statusCode: 'success', message: "No existe el usuario que intentamos validar" };

                default: throw { statusCode, message };
            }
        }

        return { statusCode: 'success', data };
    
    } catch(err) {

        console.log({ step: 'controller ValidateProduct', err: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}