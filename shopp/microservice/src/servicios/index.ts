import * as Models from '../models';
import * as T from '../types';
import { InternalError } from '../settings';
import * as Validation from 'validate-shopp/dist';
import * as Controller from '../controller/';

export async function create(params: T.Services.Create.Request): Promise<T.Services.Create.Response> {

    try {

        await Validation.create(params);

        // Validacion del usuario
        const user = (await Controller.ValidateUser({ user: params.user })).data;

        if (!user.state) {
            
            throw { statusCode: 'ValidationError', message: 'El usuario no esta habilitado para realizar compras' };
        }

        // Validacion del producto
        const product = (await Controller.ValidateProduct({ product: params.product })).data;

        if (!product.state) {
            
            throw { statusCode: 'ValidationError', message: 'No hay stock para realizar ventas' };
        }

        const { statusCode, data, message } = await Models.create(params);

        return { statusCode, data, message };
    
    } catch(err) {

        console.error({ statusCode: 'service Create', message: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function del(params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> {

    try {

        await Validation.del(params);

        const where: T.Models.Where = {};
        const optionals: T.Models.Attributes[] = ['id', 'user', 'product'];

        for (let x of optionals.map(v => v.concat('s') )) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];

        const { statusCode, data, message } = await Models.del({ where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data };
    
    } catch(err) {

        console.error({ statusCode: 'service delete', message: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function view(params: T.Services.View.Request): Promise<T.Services.View.Response> {

    try {

        await Validation.view(params);
        
        const where: T.Models.Where = {};
        const optionals: T.Models.Attributes[] = ['user', 'product'];

        for (let x of optionals.map(v => v.concat('s') )) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];

        const { statusCode, data, message } = await Models.findAndCountAll({ where });

        return { statusCode: 'success', data, message };
    
    } catch(err) {

        console.error({ statusCode: 'service view', message: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}