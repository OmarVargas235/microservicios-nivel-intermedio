type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export namespace Create {

    export interface Request {
        user: string;
        product: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}

export namespace Delete {

    export interface Request {
        ids?: number[];

        users?: string[];
        product?: string[];
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request
        message?: string;
    }
}

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;

        users?: string[];
        product?: string[];
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Request;
        message?: string;
    }
}