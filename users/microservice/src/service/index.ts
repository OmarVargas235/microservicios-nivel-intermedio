import * as Models from "../models";
import { InternalError } from "../settings";
import * as T from "../types";

import * as Validation from "validate-user";

export async function create(params: T.Service.Create.Request) : Promise<T.Service.Create.Response>{

    try {

        await Validation.create(params);

        const findOne = await Models.findOne({ where: { username : params.username } });

        if(findOne.statusCode !== 'notFound'){

            switch(findOne.statusCode){

                case 'success':  return { statusCode: 'validationError', message: 'El usuario ya se encuentra registrado'};

                default: return { statusCode: 'error', message: InternalError};

            }

        };   
        
        const { statusCode, data, message } = await Models.create(params)

        return { statusCode, data, message }

    } catch (error) {

        console.error( { step: 'Service Create', error: error.toString() } );
        

        return { statusCode: 'error', message: InternalError }
    }
};

export async function del(params: T.Service.Delete.Request): Promise<T.Service.Delete.Response> {

    try {

        await Validation.del(params);

        let where:T.Models.Where  = { username : params.username }

        const findOne = await Models.findOne({ where });

        if(findOne.statusCode !== 'success'){

            switch(findOne.statusCode){

                case 'notFound':  return { statusCode: 'validationError', message: 'El usuario no se encuentra registrado'};

                default: return { statusCode: 'error', message: InternalError};

            }

        };   
        
        const { statusCode, message } = await Models.del( { where })

        if (statusCode !== 'success') return { statusCode, message }

        return { statusCode: 'success', data: findOne.data }

    } catch (error) {

        console.error( { step: 'Service Delete', error: error.toString() } );
        
        return { statusCode: 'error', message: InternalError }

    }
};

export async function update(params: T.Service.Update.Request): Promise<T.Service.Update.Response> {

    try {

        await Validation.update(params);

        let where:T.Models.Where  = { username : params.username }

        const findOne = await Models.findOne({ where });

        if(findOne.statusCode !== 'success'){

            switch(findOne.statusCode){

                case 'notFound':  return { statusCode: 'validationError', message: 'El usuario no se encuentra registrado'};

                default: return { statusCode: 'error', message: InternalError};

            }

        };

        if(!findOne.data.state){
            
            return { statusCode: 'notPermitted', message: 'El usuario no se encuentra habilitado' }
       
        }   
        
        const { statusCode, data,  message } = await Models.update( params, { where })

        if (statusCode !== 'success') return { statusCode, message }

        return { statusCode: 'success', data: data[1][0] }

    } catch (error) {

        console.error( { step: 'Service Update', error: error.toString() } );
        
        return { statusCode: 'error', message: InternalError }

    }
};

export async function view(params: T.Service.View.Request): Promise<T.Service.View.Response> {

    try {

        await Validation.view(params);

        let where:T.Models.Where  = {};

        var optionals: T.Models.attributes[] = ['state']

        for(let x of optionals) if( params[x] !== undefined ) where[x] = params[x];

        const { statusCode, data, message } = await Models.findAndCountAll({ where });


        return { statusCode , data , message}

    } catch (error) {

        console.error( { step: 'Service View', error: error.toString() } );
        
        return { statusCode: 'error', message: InternalError }

    }
};

export async function findOne(params: T.Service.FindOne.Request): Promise<T.Service.FindOne.Response> {

    try {

        await Validation.findOne(params);

        let where:T.Models.Where  = {};

        var optionals: T.Models.attributes[] = ['id', 'username'];

        for(let x of optionals) if( params[x] !== undefined ) where[x] = params[x];
        
        const { statusCode, data,  message } = await Models.findOne( { where });

        return { statusCode, data, message };

    } catch (error) {

        console.error( { step: 'Service FindOne', error: error.toString() } );
        
        return { statusCode: 'error', message: InternalError }

    }
};


