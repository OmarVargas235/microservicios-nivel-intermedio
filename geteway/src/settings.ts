import dotenv from 'dotenv';

dotenv.config();

interface redis {
    host: string, port: number, password: string
}

export const redis: redis = {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASSWORD,
};


export const InternalError: string = 'No podemos procesar tu solicitud en estos momentos';