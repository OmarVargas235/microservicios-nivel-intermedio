type StatusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export const endpoint = [ 'create', 'update', 'delete', 'findOne', 'view' ] as const;

export type Endpoint = typeof endpoint[number];

export interface REDIS {
    host: string;
    port: number;
    password: string;
}

export interface Model {
    id?: number;
    fullName?: string,
    image?: string,
    phone?: string,
    createdAt?: string;
    uodatedAt?: string;
    userName?: string;
    password?: string;
    state?: boolean;
}

export interface IPaginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
}

export namespace Create {

    export interface Request {
        fullName: string;
        phone: string;
        image: string;
        userName?: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model | string;
        message?: string;
    }
}

export namespace Delete {

    export interface Request {
        userName: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: string | Model;
        message?: string;
    }
}

export namespace Update {

    export interface Request {
        fullName?: string;
        phone?: string;
        image?: string;
        userName: string;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: string | Model;
        message?: string;
    }
}

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;
        state?: boolean;
    }

    export interface Response {
        statusCode: StatusCode;
        data?: IPaginate;
        message?: string;
    }
}

export namespace FindOne {

    export interface Request {
        id: number | string,
    }

    export interface Response {
        statusCode: StatusCode;
        data?: Model;
        message?: string;
    }
}