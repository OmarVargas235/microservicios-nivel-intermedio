import * as T from './types';
import Joi from 'joi';

export async function create(params: T.Create.Request): Promise<T.Create.Response> {

    try {

        const schema = Joi.object({
            name: Joi.string().required(),
            mark: Joi.string().required(),
            
            year: Joi.date(),
            image: Joi.string().uri(),
            bodega: Joi.string(),
            lote: Joi.number(),
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {

    try {

        const schema = Joi.object({

            ids: Joi.array().items(Joi.number().required()),
            
            marks: Joi.array().items(Joi.string().required()),
            
            years: Joi.array().items(Joi.string().required()),

            bodegas: Joi.array().items(Joi.string().required()),
            lotes: Joi.array().items(Joi.number().required()),

            state: Joi.boolean(),
            
        }).or('ids', 'marks', 'years', 'bodega', 'lotes', 'state');

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}

export async function update(params: T.Update.Request): Promise<T.Update.Response> {

    try {

        const schema = Joi.object({
            id: Joi.number().required(),

            name: Joi.string(),
            mark: Joi.string(),

            year: Joi.string(),

            image: Joi.string().uri(),

            bodega: Joi.string(),
            lote: Joi.number(),

            state: Joi.boolean(),
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}

export async function view(params: T.View.Request): Promise<T.View.Response> {

    try {

        const schema = Joi.object({

            offset: Joi.number(),
            limit: Joi.number(),
            
            years: Joi.string().required(),
            state: Joi.boolean(),
            
            marks: Joi.array().items(Joi.string().required()),
            bodegas: Joi.array().items(Joi.string().required()),
            lotes: Joi.array().items(Joi.number().required()),
        });

        const result = await schema.validateAsync(params);
        
        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}

export async function findOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {

    try {

        const schema = Joi.object({
            id: Joi.number().required(),
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };
    
    } catch(err) {

        throw { statusCode: 'error', message: err.toString() };
    }
}