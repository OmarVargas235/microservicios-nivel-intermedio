import * as S from 'sequelize';
import { sequelize, name } from '../settings';
import { Models as T } from '../types';

export const Model = sequelize.define<T.Model>(name, {

    name: { type: S.DataTypes.STRING },
    mark: { type: S.DataTypes.STRING },
    year: { type: S.DataTypes.BIGINT },
    bodega: { type: S.DataTypes.STRING },
    code: { type: S.DataTypes.STRING },
    state: { type: S.DataTypes.STRING },
    lote: { type: S.DataTypes.BOOLEAN, defaultValue: true },

}, { freezeTableName: true });

export async function count(options?: T.Count.Request): Promise<T.Count.Response> {
    
    try {

        const instance: number = await Model.count(options);

        return { statusCode: 'success', data: instance };

    } catch(err) {

        console.error({ step: 'Models count', error: err.toString() });

        return { statusCode: 'error', message: err.toString() };
    }
}

export async function create(values: T.Create.Request, options?: T.Create.Opts): Promise<T.Create.Response> {
    
    try {

        const instance: T.Model = await Model.create(values, options);

        return { statusCode: 'success', data: instance.toJSON() };

    } catch(err) {

        console.error({ step: 'Models create', error: err.toString() });

        return { statusCode: 'error', message: err.toString() };
    }
}

export async function del(options?: T.Delete.Opts): Promise<T.Delete.Response> {
    
    try {

        const instance: number = await Model.destroy(options);

        return { statusCode: 'success', data: instance };

    } catch(err) {

        console.error({ step: 'Models del', error: err.toString() });

        return { statusCode: 'error', message: err.toString() };
    }
}

export async function findAndCountAll(options?: T.FindAndCountAll.Opts): Promise<T.FindAndCountAll.Response> {
    
    try {

        const newOptions: T.FindAndCountAll.Opts = { ...options, limit: 12, offset: 0 };
        const { count, rows }: { count: number; rows: T.Model[]; } = await Model.findAndCountAll(newOptions);
        
        return {
            statusCode: 'success',
            data: { data: rows.map(v => v.toJSON() ), itemCount: count, pageCount: Math.ceil(count / newOptions.limit)}
        };

    } catch(err) {

        console.error({ step: 'Models view', error: err.toString() });

        return { statusCode: 'error', message: err.toString() };
    }
}

export async function findOne(options?: T.FindOne.Opts): Promise<T.FindOne.Response> {
    
    try {

        const instance: T.Model | null = await Model.findOne(options);

        return instance
            ? { statusCode: 'success', data: instance.toJSON() }
            : { statusCode: 'notFound', message: 'not found' };

    } catch(err) {

        console.error({ step: 'Models findOne', error: err.toString() });

        return { statusCode: 'error', message: err.toString() };
    }
}

export async function update(values: T.Update.Request, options: T.Update.Opts): Promise<T.Update.Response> {
    
    try {

        const newOptions: T.Update.Opts = { returning: true, ...options };

        const instance: [number, T.Model[]] = await Model.update(values, newOptions);

        return { statusCode: 'success', data: [instance[0], instance[1].map(v => v.toJSON())] };

    } catch(err) {

        console.error({ step: 'Models update', error: err.toString() });

        return { statusCode: 'error', message: err.toString() };
    }
}

export async function SyncDB(options: T.SyncDB.Request): Promise<T.SyncDB.Response> {
    
    try {

        const model: T.Model = await Model.sync(options);

        return { statusCode: 'success', data: model };

    } catch(err) {

        console.error({ step: 'Models SyncDB', error: err.toString() });

        return { statusCode: 'error', message: err.toString() };
    }
}