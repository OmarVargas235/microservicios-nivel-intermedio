import * as Models from '../models';
import * as T from '../types';
import { InternalError } from '../settings';
import * as Validation from 'validate-product/dist';

export async function create(params: T.Services.Create.Request): Promise<T.Services.Create.Response> {

    try {

        await Validation.create(params);


        const { statusCode, data, message } = await Models.create(params);

        return { statusCode, data, message };
    
    } catch(err) {

        console.error({ statusCode: 'service Create', message: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function del(params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> {

    try {

        await Validation.del(params);

        const where: T.Models.Where = {};
        const optionals: T.Models.Attributes[] = ['id', 'mark', 'year', 'bodega', 'lote'];

        if (params.state !== undefined) where.state = params.state;

        for (let x of optionals.map(v => v.concat('s') )) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];

        const { statusCode, data, message } = await Models.del({ where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data };
    
    } catch(err) {

        console.error({ statusCode: 'service delete', message: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function update(params: T.Services.Update.Request): Promise<T.Services.Update.Response> {

    try {

        await Validation.update(params);

        const where: T.Models.Where = { id: params.id };
        const findOne = await Models.findOne({ where });

        if (findOne.statusCode !== 'success') {

            switch(findOne.statusCode) {

                case 'notFound': return { statusCode: 'validationError', data: "Vino no esta registrado" };

                default: return { statusCode: 'error', data: InternalError };
            }
        }

        const { statusCode, data, message } = await Models.update(params, { where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: data[1][0] };
    
    } catch(err) {

        console.error({ statusCode: 'service update', message: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function view(params: T.Services.View.Request): Promise<T.Services.View.Response> {

    try {

        await Validation.view(params);
        
        const where: T.Models.Where = {};
        const optionals: T.Models.Attributes[] = ['mark', 'bodega', 'lote'];

        for (let x of optionals.map(v => v.concat('s') )) if (params[x] !== undefined) where[x.slice(0, -1)] = params[x];

        const optionals2: T.Models.Attributes[] = ['year', 'state'];

        for (let x of optionals2) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data, message } = await Models.findAndCountAll({ where });

        return { statusCode: 'success', data, message };
    
    } catch(err) {

        console.error({ statusCode: 'service view', message: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}

export async function findOne(params: T.Services.FindOne.Request): Promise<T.Services.FindOne.Response> {

    try {

        await Validation.findOne(params);

        const where: T.Models.Where = { id: params.id };

        const { statusCode, data, message } = await Models.findOne({ where });

        return { statusCode: 'success', data: data, message };
    
    } catch(err) {

        console.error({ statusCode: 'service findOne', message: err.toString() });

        return { statusCode: 'error', message: InternalError };
    }
}